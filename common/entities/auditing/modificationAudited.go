package auditing

type ModificationAudited struct {
	LastModifierUserId uint
}
