package auditing

import (
	"core/helpers/authHelpers"
	"gorm.io/gorm"
)

type AuditedEntity struct {
	CreationAudited
	ModificationAudited
	DeletionAudited
}

func (ae *AuditedEntity) BeforeCreate(tx *gorm.DB) (err error) {
	ae.CreatorUserId = authHelpers.GetUserIdFromContext()
	return
}

func (ae *AuditedEntity) BeforeUpdate(tx *gorm.DB) (err error) {
	ae.LastModifierUserId = authHelpers.GetUserIdFromContext()
	return
}

func (ae *AuditedEntity) BeforeDelete(tx *gorm.DB) (err error) {
	ae.IsDeleted = true
	ae.DeleterUserId = authHelpers.GetUserIdFromContext()
	return
}
