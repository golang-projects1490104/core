package auditing

type DeletionAudited struct {
	DeleterUserId uint
	IsDeleted     bool
}
