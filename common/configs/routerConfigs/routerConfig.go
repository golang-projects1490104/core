package routerConfigs

import (
	"core/accounts"
	"core/accounts/application/services"
	"core/common/configs/appSettingsConfigs"
	"core/helpers/jwtHelpers"
	"core/middlewares"
	"core/roles"
	role "core/roles/application/services"
	"core/users"
	user "core/users/application/services"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
	"strings"
)

type RouterConfig struct {
	DataContext     *gorm.DB
	ApplicationPort string
	Cors            appSettingsConfigs.Cors
	TokenConfig     appSettingsConfigs.TokenConfig
	Validate        *validator.Validate
}

func (config *RouterConfig) InitRouter() {
	gin.ForceConsoleColor()

	router := gin.Default()
	router.SetTrustedProxies(strings.Split(config.Cors.TrustedProxies, ","))

	routerGroup := router.Group("/api/v1/")
	accounts.NewAccountController(services.AccountServiceImpl{DataContext: config.DataContext},
		jwtHelpers.JwtHelperImpl{TokenConfig: config.TokenConfig}, routerGroup)

	routerGroup.Use(corsMiddleWare(config.Cors))
	routerGroup.Use(middlewares.JwtAuthMiddleware(jwtHelpers.JwtHelperImpl{TokenConfig: config.TokenConfig}))

	roles.NewRoleController(role.RoleServiceImpl{DataContext: config.DataContext}, routerGroup)
	users.NewUserController(user.UserServiceImpl{DataContext: config.DataContext}, routerGroup)

	router.Run(fmt.Sprintf(":%v", config.ApplicationPort))
}

func corsMiddleWare(cors appSettingsConfigs.Cors) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", cors.Origin)
		c.Writer.Header().Set("Access-Control-Allow-Credentials", cors.Credential)
		c.Writer.Header().Set("Access-Control-Allow-Headers", cors.Header)
		c.Writer.Header().Set("Access-Control-Allow-Methods", cors.Method)

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
