package validatorConfigs

import (
	"core/users/application/validators"
	"github.com/go-playground/validator/v10"
)

type ValidatorConfig struct {
	Validate *validator.Validate
}

func (validatorConfig *ValidatorConfig) RegisterCustomValidator() {
	validatorConfig.Validate.RegisterValidation("emailChecker", validators.EmailChecker)
}
