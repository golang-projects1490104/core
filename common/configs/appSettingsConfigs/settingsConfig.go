package appSettingsConfigs

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Config struct {
	Environment     string
	ApplicationPort string
	Database        Database
	Cors            Cors
	Token           TokenConfig
}

type TokenConfig struct {
	TokenLifeTime int
	ApiSecretKey  string
}

type Cors struct {
	Origin         string
	Method         string
	Header         string
	Credential     string
	TrustedProxies string
}

type Database struct {
	Host     string
	Port     uint
	Name     string
	User     string
	Password string
	TimeZone string
}

func (config *Config) ConnectionString() string {
	var dbConfig = config.Database
	var connectionString = fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable TimeZone=%v",
		dbConfig.Host, dbConfig.User, dbConfig.Password, dbConfig.Name, dbConfig.Port, dbConfig.TimeZone)

	return connectionString
}

func LoadConfig(filePath string, environment string) *Config {
	viper.AddConfigPath(filePath)
	viper.SetConfigName(fmt.Sprintf("appSettings.%s", environment))

	viper.SetConfigType("yaml")
	viper.ReadInConfig()

	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.WatchConfig()

	var config *Config

	err := viper.Unmarshal(&config)
	if err != nil {
		fmt.Println("Unmarshalling failed!")
	}

	return config
}
