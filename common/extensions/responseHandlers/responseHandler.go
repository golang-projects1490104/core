package responseHandlers

import (
	"core/common/extensions/exceptionHandlers"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Result struct {
	StatusCode int         `json:"statusCode"`
	Data       interface{} `json:"data"`
	Error      interface{} `json:"error"`
}

func Handle(data interface{}, err error, context *gin.Context) {
	if err != nil {
		var error = exceptionHandlers.GetExceptionDetails(err)
		context.JSON(error.StatusCode, Result{
			Error:      error,
			StatusCode: error.StatusCode,
		})
		return
	}

	context.JSON(http.StatusOK, Result{
		Data:       data,
		StatusCode: http.StatusOK,
	})
}
