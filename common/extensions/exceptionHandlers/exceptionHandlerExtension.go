package exceptionHandlers

import (
	"core/common/exceptions"
	"net/http"
)

const (
	NotFound            = "NOT_FOUND_ERROR"
	InternalServerError = "INTERNAL_SERVER_ERROR"
)

type Error struct {
	StatusCode int    `json:"statusCode"`
	Code       string `json:"code"`
	Message    string `json:"message"`
}

func GetExceptionDetails(err error) Error {
	var errorDetail Error

	switch err := err.(type) {
	case exceptions.NotFoundException:
		errorDetail = Error{
			Code:       NotFound,
			Message:    err.Message,
			StatusCode: http.StatusNotFound,
		}
	default:
		errorDetail = Error{
			Code:       InternalServerError,
			Message:    err.Error(),
			StatusCode: http.StatusInternalServerError,
		}
	}
	return errorDetail
}
