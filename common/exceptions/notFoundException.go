package exceptions

type NotFoundException struct {
	Code    int
	Status  string
	Message string
}

func (n NotFoundException) Error() string {
	return n.Message
}
