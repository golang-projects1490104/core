package users

import (
	"core/common/extensions/responseHandlers"
	"core/users/application/dtos"
	"core/users/application/services"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type UserController struct {
	Router      *gin.RouterGroup
	UserService services.IUserService
}

func NewUserController(userService services.UserServiceImpl, router *gin.RouterGroup) {
	userController := &UserController{
		Router:      router,
		UserService: userService,
	}

	router.GET("users/:id", userController.GetById)
	router.POST("users", userController.Create)
	router.PUT("users/:id", userController.Update)
	router.DELETE("users/:id", userController.Delete)
	router.PATCH("users/:id/activate", userController.Activate)
	router.PATCH("users/:id/deactivate", userController.Deactivate)
}

func (uc UserController) GetById(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	user, err := uc.UserService.GetById(uint(id))

	responseHandlers.Handle(user, err, context)
}

func (uc UserController) Create(context *gin.Context) {
	var userCreateDto dtos.UserCreateDto

	err := context.ShouldBindJSON(&userCreateDto)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = uc.UserService.Create(userCreateDto)
	responseHandlers.Handle(nil, err, context)
}

func (uc UserController) Update(context *gin.Context) {
	var userUpdateDto dtos.UserUpdateDto
	var id, _ = strconv.Atoi(context.Param("id"))

	err := context.ShouldBindJSON(&userUpdateDto)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = uc.UserService.Update(uint(id), userUpdateDto)
	responseHandlers.Handle(nil, err, context)
}

func (uc UserController) Delete(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := uc.UserService.Delete(uint(id))

	responseHandlers.Handle(nil, err, context)
}

func (uc UserController) Activate(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := uc.UserService.Activate(uint(id))

	responseHandlers.Handle(nil, err, context)
}

func (uc UserController) Deactivate(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := uc.UserService.Deactivate(uint(id))

	responseHandlers.Handle(nil, err, context)
}
