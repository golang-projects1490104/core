package models

import "gorm.io/gorm"

type UserRole struct {
	gorm.Model
	RoleId uint
	UserId uint
}
