package models

import (
	"core/common/entities/auditing"
	"core/roles/domain/models"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	auditing.AuditedEntity
	FirstName    string
	LastName     string
	MiddleName   string
	EmailAddress string
	UserName     string
	PhoneNumber  string
	PasswordHash string
	IsSuperAdmin bool `gorm:"default:false"`
	IsActive     bool `gorm:"default:true"`

	Roles []models.Role `gorm:"many2many:user_roles;"`
}
