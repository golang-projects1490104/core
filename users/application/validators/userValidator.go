package validators

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"regexp"
)

var EmailChecker validator.Func = func(fl validator.FieldLevel) bool {
	var email = fl.Field().String()
	fmt.Println(email)
	match, _ := regexp.MatchString("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$", email)

	return match
}
