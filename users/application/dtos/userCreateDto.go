package dtos

type UserCreateDto struct {
	Roles        []uint `json:"roles" binding:"required"`
	FirstName    string `json:"firstName" binding:"required"`
	LastName     string `json:"lastName" binding:"required"`
	MiddleName   string `json:"middleName" binding:"required"`
	UserName     string `json:"userName" binding:"required"`
	EmailAddress string `json:"emailAddress" binding:"required"`
	PhoneNumber  string `json:"phoneNumber" binding:"required"`
	Password     string `json:"password" binding:"required"`
}
