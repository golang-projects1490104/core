package dtos

type UserDto struct {
	Id           uint
	FirstName    string
	LastName     string
	MiddleName   string
	EmailAddress string
	UserName     string
	PhoneNumber  string
	IsActive     bool
	Roles        []uint
}
