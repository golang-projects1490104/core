package services

import (
	"core/common/exceptions"
	"core/helpers/passwordHelpers"
	"core/users/application/dtos"
	"core/users/domain/models"
	"gorm.io/gorm"
)

type IUserService interface {
	GetById(id uint) (dtos.UserDto, error)
	Create(userCreateDto dtos.UserCreateDto) error
	Update(id uint, userUpdateDto dtos.UserUpdateDto) error
	Delete(id uint) error
	Activate(id uint) error
	Deactivate(id uint) error
}

type UserServiceImpl struct {
	DataContext *gorm.DB
}

func (us UserServiceImpl) GetById(id uint) (dtos.UserDto, error) {
	var user models.User

	var err = us.DataContext.Preload("Roles").First(&user, id).Error
	if err != nil {
		return dtos.UserDto{}, exceptions.NotFoundException{Message: err.Error()}
	}

	var roles []uint
	for _, item := range user.Roles {
		roles = append(roles, item.ID)
	}

	return dtos.UserDto{
		Id: user.ID, FirstName: user.FirstName,
		LastName: user.LastName, MiddleName: user.MiddleName,
		EmailAddress: user.EmailAddress, PhoneNumber: user.PhoneNumber,
		IsActive: user.IsActive, Roles: roles, UserName: user.UserName,
	}, err
}

func (us UserServiceImpl) Create(userCreateDto dtos.UserCreateDto) error {
	dataContext := us.DataContext.Begin()
	var user = models.User{
		FirstName:    userCreateDto.FirstName,
		LastName:     userCreateDto.LastName,
		MiddleName:   userCreateDto.MiddleName,
		UserName:     userCreateDto.UserName,
		EmailAddress: userCreateDto.EmailAddress,
		PhoneNumber:  userCreateDto.PhoneNumber,
		PasswordHash: passwordHelpers.CreatePasswordHash(userCreateDto.Password),
	}

	err := dataContext.Create(&user).Error
	if err != nil {
		dataContext.Rollback()
		return err
	}

	createUserRoles(dataContext, user.ID, userCreateDto.Roles)

	dataContext.Commit()
	return nil
}

func (us UserServiceImpl) Update(id uint, userUpdateDto dtos.UserUpdateDto) error {
	dataContext := us.DataContext.Begin()

	var user = models.User{
		FirstName: userUpdateDto.FirstName, LastName: userUpdateDto.LastName,
		PhoneNumber: userUpdateDto.PhoneNumber, UserName: userUpdateDto.UserName,
		MiddleName: userUpdateDto.MiddleName, EmailAddress: userUpdateDto.EmailAddress,
	}

	err := dataContext.Model(&models.User{}).Where("Id=?", id).Select("FirstName",
		"LastName", "PhoneNumber", "UserName", "MiddleName", "EmailAddress").Updates(user).Error
	if err != nil {
		dataContext.Rollback()
		return err
	}

	updateUserRoles(dataContext, id, userUpdateDto.Roles)

	dataContext.Commit()
	return nil
}

func (us UserServiceImpl) Delete(id uint) error {
	err := us.DataContext.Delete(&models.User{}, id).Error

	return err
}

func (us UserServiceImpl) Activate(id uint) error {
	err := us.DataContext.Model(&models.User{}).Where("Id=?", id).Select("IsActive").Updates(models.User{IsActive: true}).Error

	return err
}

func (us UserServiceImpl) Deactivate(id uint) error {
	err := us.DataContext.Model(&models.User{}).Where("Id=?", id).Select("IsActive").Updates(models.User{IsActive: false}).Error

	return err
}

func createUserRoles(dataContext *gorm.DB, userId uint, roles []uint) {
	var userRoles []models.UserRole
	for _, item := range roles {
		var userRole = models.UserRole{
			RoleId: item,
			UserId: userId,
		}
		userRoles = append(userRoles, userRole)
	}

	err := dataContext.Create(&userRoles).Error
	if err != nil {
		dataContext.Rollback()
	}
}

func updateUserRoles(dataContext *gorm.DB, userId uint, roles []uint) {
	var userRoles []models.UserRole
	dataContext.Where(&models.UserRole{UserId: userId}).Find(&userRoles)

	var newUserRoles []models.UserRole
	for _, item := range roles {
		var hasUserRoleId = containsForRolePermission(userRoles, item)
		if !hasUserRoleId {
			newUserRoles = append(newUserRoles, models.UserRole{RoleId: item, UserId: userId})
		}
	}

	for _, item := range userRoles {
		var isRoleIdSelected = contains(roles, item.RoleId)
		if !isRoleIdSelected {
			err := dataContext.Unscoped().Delete(&models.UserRole{}, item.ID).Error
			if err != nil {
				dataContext.Rollback()
			}
		}
	}

	if len(newUserRoles) > 0 {
		err := dataContext.Create(&newUserRoles).Error
		if err != nil {
			dataContext.Rollback()
		}
	}
}

func containsForRolePermission(userRoles []models.UserRole, roleId uint) bool {
	for _, item := range userRoles {
		if item.RoleId == roleId {
			return true
		}
	}
	return false
}

func contains[T comparable](s []T, e T) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}
