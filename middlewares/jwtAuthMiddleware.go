package middlewares

import (
	"core/helpers/authHelpers"
	"core/helpers/jwtHelpers"
	"github.com/gin-gonic/gin"
	"net/http"
)

func JwtAuthMiddleware(jwtHelper jwtHelpers.IJwtHelper) gin.HandlerFunc {
	return func(c *gin.Context) {
		err := jwtHelper.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}
		authHelpers.NewAuthHelper(c, jwtHelper)
		c.Next()
	}
}
