package services

import (
	"core/accounts/application/dtos"
	"core/common/exceptions"
	"core/helpers/passwordHelpers"
	"core/users/domain/models"
	"gorm.io/gorm"
)

type IAccountService interface {
	SignInAsync(userSignInDto dtos.UserSignInDto) (dtos.UserSignInResponseDto, error)
}

type AccountServiceImpl struct {
	DataContext *gorm.DB
}

func (as AccountServiceImpl) SignInAsync(userSignInDto dtos.UserSignInDto) (dtos.UserSignInResponseDto, error) {
	var user models.User

	var err = as.DataContext.Where("user_name=?", userSignInDto.UserName).First(&user).Error
	if err != nil {
		return dtos.UserSignInResponseDto{}, exceptions.NotFoundException{Message: err.Error()}
	}

	var result = passwordHelpers.VerifyPassword(user.PasswordHash, userSignInDto.Password)
	if !result {
		return dtos.UserSignInResponseDto{}, exceptions.NotFoundException{Message: err.Error()}
	}

	return dtos.UserSignInResponseDto{UserId: user.ID, IsSuperAdmin: user.IsSuperAdmin}, nil
}
