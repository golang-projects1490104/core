package dtos

type UserTokenDto struct {
	AccessToken string `json:"accessToken"`
}
