package dtos

type UserSignInResponseDto struct {
	UserId       uint
	IsSuperAdmin bool
}
