package accounts

import (
	"core/accounts/application/dtos"
	"core/accounts/application/services"
	"core/common/extensions/responseHandlers"
	"core/helpers/jwtHelpers"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AccountController struct {
	Router         *gin.RouterGroup
	JwtHelper      jwtHelpers.IJwtHelper
	AccountService services.IAccountService
}

func NewAccountController(accountService services.AccountServiceImpl,
	jwtHelper jwtHelpers.JwtHelperImpl, router *gin.RouterGroup) {

	accountController := &AccountController{
		Router:         router,
		JwtHelper:      jwtHelper,
		AccountService: accountService,
	}

	router.POST("sign-in", accountController.SignIn)
}

func (ac AccountController) SignIn(context *gin.Context) {
	var userSignInDto dtos.UserSignInDto

	err := context.ShouldBindJSON(&userSignInDto)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	data, err := ac.AccountService.SignInAsync(userSignInDto)

	token, err := ac.JwtHelper.GenerateToken(data.UserId)

	responseHandlers.Handle(dtos.UserTokenDto{AccessToken: token}, err, context)
}
