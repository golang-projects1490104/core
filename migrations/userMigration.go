package migrations

import "core/users/domain/models"

func (config *Config) userMigration() {
	config.DataContext.AutoMigrate(&models.User{})
	config.DataContext.AutoMigrate(&models.UserRole{})
}
