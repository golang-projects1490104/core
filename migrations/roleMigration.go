package migrations

import (
	"core/roles/domain/models"
	"gorm.io/gorm"
	"log"
	"os"
)

func (config *Config) roleMigration() {
	createGetAllPermissionFunction(config.DataContext)
	config.DataContext.AutoMigrate(&models.Role{})
	config.DataContext.AutoMigrate(&models.Permission{})
	config.DataContext.AutoMigrate(&models.RolePermission{})
}

func createGetAllPermissionFunction(dataContext *gorm.DB) {
	sql, err := os.ReadFile("./migrations/functions/getAllPermissions_Function.txt")
	if err != nil {
		log.Println(err)
	}

	dataContext.Exec(string(sql))
}
