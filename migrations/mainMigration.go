package migrations

import (
	"gorm.io/gorm"
)

type Config struct {
	DataContext *gorm.DB
}

func NewMainMigration(context *gorm.DB) {
	config := &Config{
		DataContext: context,
	}

	config.userMigration()
	config.roleMigration()
}
