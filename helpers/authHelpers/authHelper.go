package authHelpers

import (
	"core/helpers/jwtHelpers"
	"github.com/gin-gonic/gin"
	"log"
)

var context *gin.Context
var jwtHelper jwtHelpers.IJwtHelper

func NewAuthHelper(c *gin.Context, jh jwtHelpers.IJwtHelper) {
	context = c
	jwtHelper = jh
}

func GetUserIdFromContext() uint {
	userId, err := jwtHelper.ExtractTokenID(context)

	if err != nil {
		log.Println(err)
		return 0
	}

	return userId
}
