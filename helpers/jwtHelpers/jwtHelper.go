package jwtHelpers

import (
	"core/common/configs/appSettingsConfigs"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
	"time"
)

type IJwtHelper interface {
	GenerateToken(userId uint) (string, error)
	TokenValid(context *gin.Context) error
	ExtractTokenID(context *gin.Context) (uint, error)
}

type JwtHelperImpl struct {
	TokenConfig appSettingsConfigs.TokenConfig
}

func (jwtHelper JwtHelperImpl) GenerateToken(userId uint) (string, error) {
	claims := jwt.MapClaims{
		"userId":     userId,
		"authorized": true,
		"exp":        time.Now().Add(time.Hour * time.Duration(jwtHelper.TokenConfig.TokenLifeTime)).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(jwtHelper.TokenConfig.ApiSecretKey))
}

func (jwtHelper JwtHelperImpl) TokenValid(context *gin.Context) error {
	tokenString := extractToken(context)

	_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(jwtHelper.TokenConfig.ApiSecretKey), nil
	})

	if err != nil {
		return err
	}
	return nil
}

func (jwtHelper JwtHelperImpl) ExtractTokenID(context *gin.Context) (uint, error) {
	tokenString := extractToken(context)

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(jwtHelper.TokenConfig.ApiSecretKey), nil
	})

	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		uid, err := strconv.ParseUint(fmt.Sprintf("%.0f", claims["userId"]), 10, 32)
		if err != nil {
			return 0, err
		}
		return uint(uid), nil
	}
	return 0, nil
}

func extractToken(context *gin.Context) string {
	bearerToken := context.Request.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}
