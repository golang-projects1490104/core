package models

import "gorm.io/gorm"

type RolePermission struct {
	gorm.Model
	RoleId       uint
	PermissionId uint
}
