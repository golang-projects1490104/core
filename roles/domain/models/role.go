package models

import (
	"core/common/entities/auditing"
	"gorm.io/gorm"
)

type Role struct {
	gorm.Model
	auditing.AuditedEntity
	Name        string
	Description string
	IsActive    bool `gorm:"default:true"`

	Permissions []Permission `gorm:"many2many:role_permissions;"`
}
