package models

type Permission struct {
	ID          uint `gorm:"primarykey"`
	Name        string
	ParentId    uint `gorm:"foreignKey:"ID"`
	Description string

	Roles []Role `gorm:"many2many:role_permissions;"`
}
