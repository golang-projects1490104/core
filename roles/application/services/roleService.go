package services

import (
	"core/roles/application/dtos"
	"core/roles/domain/models"
	"gorm.io/gorm"
)

type IRoleService interface {
	GetAll() ([]dtos.RoleSelectItemDto, error)
	GetById(id uint) (dtos.RoleDto, error)
	GetAllPermissions() ([]dtos.PermissionSelectItemDto, error)
	Create(roleCreateDto dtos.RoleCreateDto) error
	Update(id uint, roleUpdateDto dtos.RoleUpdateDto) error
	Delete(id uint) error
	Activate(id uint) error
	Deactivate(id uint) error
}

type RoleServiceImpl struct {
	DataContext *gorm.DB
}

func (rs RoleServiceImpl) GetAll() ([]dtos.RoleSelectItemDto, error) {
	var roles []dtos.RoleSelectItemDto
	err := rs.DataContext.Table("roles").Select("id", "name").Scan(&roles).Error
	if err != nil {
		return nil, err
	}

	return roles, nil
}

func (rs RoleServiceImpl) GetById(id uint) (dtos.RoleDto, error) {
	var role models.Role
	err := rs.DataContext.Preload("Permissions").First(&role, id).Error
	if err != nil {
		return dtos.RoleDto{}, err
	}

	var permissions []uint
	for _, item := range role.Permissions {
		permissions = append(permissions, item.ID)
	}

	return dtos.RoleDto{
		Id:          role.ID,
		Name:        role.Name,
		Description: role.Description,
		Permissions: permissions,
	}, nil
}

func (rs RoleServiceImpl) GetAllPermissions() ([]dtos.PermissionSelectItemDto, error) {
	var permissions []dtos.PermissionSelectItemDto
	err := rs.DataContext.Raw("select *from public.get_all_permissions()").Scan(&permissions).Error
	if err != nil {
		return nil, err
	}

	return permissions, nil
}

func (rs RoleServiceImpl) Create(roleCreateDto dtos.RoleCreateDto) error {

	dataContext := rs.DataContext.Begin()

	var role = models.Role{
		Name:        roleCreateDto.Name,
		Description: roleCreateDto.Description,
	}

	err := dataContext.Create(&role).Error
	if err != nil {
		dataContext.Rollback()
		return err
	}

	createRolePermission(dataContext, role.ID, roleCreateDto.Permissions)

	dataContext.Commit()
	return nil
}

func (rs RoleServiceImpl) Update(id uint, roleUpdateDto dtos.RoleUpdateDto) error {
	dataContext := rs.DataContext.Begin()

	var role = models.Role{Name: roleUpdateDto.Name, Description: roleUpdateDto.Description}
	err := dataContext.Model(&models.Role{}).Where("Id=?", id).Select("Name", "Description").Updates(role).Error

	if err != nil {
		dataContext.Rollback()
		return err
	}

	updateRolePermission(dataContext, id, roleUpdateDto.Permissions)

	dataContext.Commit()
	return nil
}

func (rs RoleServiceImpl) Delete(id uint) error {
	err := rs.DataContext.Delete(&models.Role{}, id).Error
	return err
}

func (rs RoleServiceImpl) Activate(id uint) error {
	err := rs.DataContext.Model(&models.Role{}).Where("Id=?", id).Select("IsActive").Updates(models.Role{IsActive: true}).Error

	return err
}

func (rs RoleServiceImpl) Deactivate(id uint) error {
	err := rs.DataContext.Model(&models.Role{}).Where("Id=?", id).Select("IsActive").Updates(models.Role{IsActive: false}).Error

	return err
}

func createRolePermission(dataContext *gorm.DB, roleId uint, permissions []uint) {
	var rolePermissions []models.RolePermission
	for _, item := range permissions {
		var rolePermission = models.RolePermission{
			RoleId:       roleId,
			PermissionId: item,
		}
		rolePermissions = append(rolePermissions, rolePermission)
	}

	err := dataContext.Create(&rolePermissions).Error
	if err != nil {
		dataContext.Rollback()
	}
}

func updateRolePermission(dataContext *gorm.DB, roleId uint, permissions []uint) {
	var rolePermissions []models.RolePermission
	dataContext.Where(&models.RolePermission{RoleId: roleId}).Find(&rolePermissions)

	var newRolePermissions []models.RolePermission
	for _, item := range permissions {
		var hasRolePermissionId = containsForRolePermission(rolePermissions, item)
		if !hasRolePermissionId {
			newRolePermissions = append(newRolePermissions, models.RolePermission{RoleId: roleId, PermissionId: item})
		}
	}

	for _, item := range rolePermissions {
		var isPermissionIdSelected = contains(permissions, item.PermissionId)
		if !isPermissionIdSelected {
			err := dataContext.Unscoped().Delete(&models.RolePermission{}, item.ID).Error
			if err != nil {
				dataContext.Rollback()
			}
		}
	}

	if len(newRolePermissions) > 0 {
		err := dataContext.Create(&newRolePermissions).Error
		if err != nil {
			dataContext.Rollback()
		}
	}
}

func containsForRolePermission(rolePermissions []models.RolePermission, permissionId uint) bool {
	for _, item := range rolePermissions {
		if item.PermissionId == permissionId {
			return true
		}
	}
	return false
}

func contains[T comparable](s []T, e T) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}
