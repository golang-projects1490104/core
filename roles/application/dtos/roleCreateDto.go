package dtos

type RoleCreateDto struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description"`
	Permissions []uint `json:"permissions" binding:"required"`
}
