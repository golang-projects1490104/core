package dtos

type RoleDto struct {
	Id          uint
	Name        string
	Description string
	Permissions []uint
}
