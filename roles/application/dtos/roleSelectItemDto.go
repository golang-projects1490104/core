package dtos

type RoleSelectItemDto struct {
	Id    uint   `gorm:"column:id"`
	Value string `gorm:"column:name"`
}
