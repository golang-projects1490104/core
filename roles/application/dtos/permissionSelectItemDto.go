package dtos

type PermissionSelectItemDto struct {
	Id    uint   `gorm:"column:perm_id"`
	Value string `gorm:"column:perm_name"`
}
