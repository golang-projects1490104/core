package roles

import (
	"core/common/extensions/responseHandlers"
	"core/roles/application/dtos"
	"core/roles/application/services"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type RoleController struct {
	Router      *gin.RouterGroup
	RoleService services.IRoleService
}

func NewRoleController(RoleService services.RoleServiceImpl, router *gin.RouterGroup) {
	roleController := &RoleController{
		Router:      router,
		RoleService: RoleService,
	}

	router.GET("roles/all", roleController.GetAll)
	router.GET("roles/:id", roleController.GetById)
	router.GET("permissions/all", roleController.GetAllPermissions)
	router.POST("roles", roleController.Create)
	router.PUT("roles/:id", roleController.Update)
	router.DELETE("roles/:id", roleController.Delete)
	router.PATCH("roles/:id/activate", roleController.Activate)
	router.PATCH("roles/:id/deactivate", roleController.Deactivate)
}

func (rc RoleController) GetAll(context *gin.Context) {
	var roles, err = rc.RoleService.GetAll()

	responseHandlers.Handle(roles, err, context)
}

func (rc RoleController) GetById(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	var role, err = rc.RoleService.GetById(uint(id))

	responseHandlers.Handle(role, err, context)
}

func (rc RoleController) GetAllPermissions(context *gin.Context) {
	var permissions, err = rc.RoleService.GetAllPermissions()

	responseHandlers.Handle(permissions, err, context)
}

func (rc RoleController) Create(context *gin.Context) {
	var roleCreateDto dtos.RoleCreateDto

	err := context.ShouldBindJSON(&roleCreateDto)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = rc.RoleService.Create(roleCreateDto)
	responseHandlers.Handle(nil, err, context)
}

func (rc RoleController) Update(context *gin.Context) {
	var roleUpdateDto dtos.RoleUpdateDto
	var id, _ = strconv.Atoi(context.Param("id"))

	err := context.ShouldBindJSON(&roleUpdateDto)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = rc.RoleService.Update(uint(id), roleUpdateDto)
	responseHandlers.Handle(nil, err, context)
}

func (rc RoleController) Delete(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := rc.RoleService.Delete(uint(id))
	responseHandlers.Handle(nil, err, context)
}

func (rc RoleController) Activate(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := rc.RoleService.Activate(uint(id))
	responseHandlers.Handle(nil, err, context)
}

func (rc RoleController) Deactivate(context *gin.Context) {
	var id, _ = strconv.Atoi(context.Param("id"))
	err := rc.RoleService.Deactivate(uint(id))
	responseHandlers.Handle(nil, err, context)
}
