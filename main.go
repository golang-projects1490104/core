package main

import (
	"core/common/configs/appSettingsConfigs"
	"core/common/configs/dataConfigs"
	"core/common/configs/routerConfigs"
	"core/migrations"
	"github.com/go-playground/validator/v10"
)

func main() {
	var validate = validator.New()
	var config = appSettingsConfigs.LoadConfig("./appSettings", "development")
	var dataContext = dataConfigs.ConnectToDatabase(config.ConnectionString())

	//Create all Table
	migrations.NewMainMigration(dataContext)

	//Init all REST API routes
	var routerConfig = routerConfigs.RouterConfig{
		Validate:        validate,
		Cors:            config.Cors,
		DataContext:     dataContext,
		TokenConfig:     config.Token,
		ApplicationPort: config.ApplicationPort,
	}
	routerConfig.InitRouter()
}
